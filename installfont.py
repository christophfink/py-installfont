#!/usr/bin/env python
# -*- coding: utf-8 -* 

from __future__ import print_function
import os, os.path
from shutil import copy
from platform import system as platform_system
from magic import from_file as mimetype

def installfont(fontfile):
	if not os.path.isfile(fontfile):
		raise NameError("{} is not a valid font file")
	try:
		from magic import from_file 
		mimetype=magic.from_file(fontfile,mime=True)
	except ImportError:
		raise ImportWarning("Could not import python module \"magic\" – not checking file type of font_file")
		mimetype="not/checked"

	if mimetype not in ["application/x-font-ttf","application/vnd.ms-opentype","not/checked"] or os.path.splitext(fontfile)[1] not in [".ttf",".otf"]:
		raise TypeError("{} is not a valid font file")
	
	platform=platform_system()
	if platform not in ["Linux","Darwin","Windows"]:
		raise NotImplementedError("Sorry, I do not know how to install fonts on a \"{}\" platform.".format(platform))
	
	if platform=="Linux":
		fontDir=os.path.join(
			os.path.expanduser("~"),
			".fonts"
		)
		if not os.path.isdir(fontDir):
			os.mkdir(fontDir)
		copy(fontfile,fontDir)
	elif platform=="Darwin":
		fontDir=os.path.join(
			os.path.expanduser("~"),
			"Library",
			"Fonts"
		)
		if not os.path.isdir(fontDir):
			os.mkdir(fontDir)
		copy(fontfile,fontDir)
	elif platform=="Windows":
		import win32api
		import win32con
		import ctypes
		ctypes.windll.gdi32.AddFontResourceA(fontfile)
		win32api.SendMessage(win32con.HWND_BROADCAST, win32con.WM_FONTCHANGE)
	

def main():
	import sys
	try:
		fontfile=sys.argv[1]
		installfont(fontfile)
	except NameError,e:
		print("File not found: {}".format(e))
	except TypeError,e:
		print("Error installing font: {}".format(e))
	except:
		print("Usage: {} [fontfile]".format(basename(sys.argv[0])))


if __name__=="__main__":
	main()
